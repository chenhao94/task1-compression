#include "Distance.h"

double geo_dist(double lon1, double lat1, double lon2, double lat2)
{
	double radLat1 = rad(lat1);
	double radLat2 = rad(lat2);
	double delta_lon = rad(lon2 - lon1);
	double top_1 = cos(radLat2) * sin(delta_lon);
	double top_2 = cos(radLat1) * sin(radLat2) - sin(radLat1) * cos(radLat2) * cos(delta_lon);
	double top = sqrt(top_1 * top_1 + top_2 * top_2);
	double bottom = sin(radLat1) * sin(radLat2) + cos(radLat1) * cos(radLat2) * cos(delta_lon);
	double delta_sigma = atan2(top, bottom);
	double distance = delta_sigma * 6378137.0;
	return distance;
}

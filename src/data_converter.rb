require 'time'
def get_timestamp(str)
	timestamp = Time.strptime(str, "%y-%m-%d %R:%S")
	timestamp -= Time.strptime("14-10-07 03:00:00", "%y-%m-%d %R:%S")
end

def lon_to_f(lon)
	lon = lon.to_f/100
	lon_i = lon.truncate
	lon = (lon - lon_i)*5/3 + lon_i
	return ((lon.to_f-116)*100000).to_f
end

def lat_to_f(lat)
	lat = lat.to_f/100
	lat_i = lat.truncate
	lat = (lat - lat_i)*5/3 + lat_i
	return ((lat.to_f-39)*100000).to_f
end

def int_to_time(x)
	(Time.strptime("14-10-07 03:00:00", "%y-%m-%d %R:%S") + x).strftime("%y-%m-%d %R:%S")
end

def f_to_lon(f)
	f.to_f/100000 + 116
end

def f_to_lat(f)
	f.to_f/100000 + 39
end

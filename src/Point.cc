#include "Point.h"
#include "Distance.h"

double inner_product(Point *p1, Point *p2, Point *p3, Point *p4)
{
	return (p2->x-p1->x)*(p4->x-p3->x) + (p2->y-p1->y)*(p4->y-p3->y);
}

Point* Point::s_project(Point *p1, Point *p2)
{
	double r = inner_product(p1, this, p1, p2)/p2->magnitude(p1);
	return new Point(id, t, p1->x+(p2->x-p1->x)*r, p1->y+(p2->y-p1->y)*r);
}

Point* Point::t_project(Point *p1, Point *p2)
{
	double r = ((double)(t-p1->t))/((double)(p2->t-p1->t));
	return new Point(id, t, p1->x+(p2->x-p1->x)*r, p1->y+(p2->y-p1->y)*r);
}
	
double Point::dist_to_line(Point *p1, Point *p2)
{
	Point* point = project(p1, p2);
	return x_y_dist(x, y, point->x, point->y);
}

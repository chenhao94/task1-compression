#ifndef __PARAM_H__
#define __PARAM_H__

extern int ERR;
extern int PROJ_TYPE;
/**
 * 	0 - temporal projection
 * 	1 - spatio projection
 */
extern int COMP_ALOG;
/**
 * 	0 - Douglas-Peucker Algorithm
 * 	1 - Sliding Window Algorithm
 * 	2 - Open Window Algorithm
 * 	3 - Dynamic Progamming
 */

extern bool RECOMPRESS;
/**
 * 	If use window algorithm, you can do recompress optionally
 */

#endif

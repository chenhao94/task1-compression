#ifndef __COMPRESS_H___
#define __COMPRESS_H___

#include <vector>
#include <stack>

#include "Point.h"

struct Frame
{
	int st, en, max;
	static std::vector<Point *> *points;

	Frame(int _st=0, int _en=0): st(_st), en(_en), max(-1) {}

	void check_max(int x)
	{
		if (max == -1)
			max = x;
		else if ((*points)[x]->dist_to_line((*points)[st], (*points)[en]) > 
			   (*points)[max]->dist_to_line((*points)[st], (*points)[en]))
			max = x;
	}

};

void compress(std::vector<Point *> &points);
void compress_DoP(std::vector<Point *> &points);
void compress_SW(std::vector<Point *> &points);
void compress_OW(std::vector<Point *> &points);
void compress_DyP(std::vector<Point *> &points);
void recompress(std::vector<Point *> &points);

#endif

#include "Compress.h"
#include "Param.h"
#include <cstring>

std::vector<Point *> *Frame::points;

void compress(std::vector<Point *> &points)
{
	switch (COMP_ALOG){
		case 0: compress_DoP(points); break;
		case 1: compress_SW(points); break;
		case 2: compress_OW(points); break;
		case 3: compress_DyP(points); break;
	}

	if ((COMP_ALOG == 1 || COMP_ALOG == 2) && RECOMPRESS)
		recompress(points);
}

void compress_DoP(std::vector<Point *> &points)
{
	// using Douglas-Peucker Alogrithm
	
	Frame::points = &points;
	
	std::stack<Frame *> segments;
	Frame *top;
	int st=0, en=points.size()-1, max;

	points[st]->reserve = true;
	points[en]->reserve = true;
	segments.push(new Frame(st, en));

	while (!segments.empty())
	{
		top = segments.top();
		segments.pop();
 		st = top->st;
		en = top->en;

		if (en - st < 2)
			continue;
		for (int i = st + 1; i < en; ++i ) top->check_max(i);
		max = top->max;

		if (points[max]->dist_to_line(points[st], points[en]) > ERR)
		{
			points[max]->reserve = true;
			segments.push(new Frame(st, max));
			segments.push(new Frame(max, en));
		}

	}
}

void compress_SW(std::vector<Point *> &points)
{
	// Using sliding window algorithm
	int st=0, en=points.size()-1, len = en + 1;
	bool valid;

	points[st]->reserve = true;
	points[en]->reserve = true;
	en = 2;

	while (en < len - 1)
	{
		valid = true;
		for (int i = st + 1; i <= en; ++i)
			if (points[i]->dist_to_line(points[st], points[en + 1]) > ERR)
			{
				valid = false;
				break;
			}
		if (valid)
			++en;
		else
		{
			points[en]->reserve = true;
			st = en;
			++en;
		}
	}
}

void compress_OW(std::vector<Point *> &points)
{
	// Using open window algorithm
	int st=0, en=points.size()-1, len = en + 1, max;
	bool valid;

	points[st]->reserve = true;
	points[en]->reserve = true;
	en = 2;

	while (en < len - 1)
	{
		valid = true;
		for (int i = st + 1; i <= en; ++i)
			if (points[i]->dist_to_line(points[st], points[en + 1]) > ERR)
			{
				valid = false;
				max = i;
				break;
			}
		if (valid)
			++en;
		else
		{
			for (int i = max + 1; i <= en; ++i)
				if (points[i]->dist_to_line(points[st], points[en + 1]) > 
				  points[max]->dist_to_line(points[st], points[en + 1]))
					max = i;
			points[max]->reserve = true;
			st = max;
			en = st + 1;
		}
	}
}

void compress_DyP(std::vector<Point *> &points)
{
	// Using Dynamic Programming
	int len = points.size();
	int *opt = new int[len], *from = new int[len];
	bool valid;
	memset(opt, 0x3f, sizeof(int)*len);

	opt[0] = 1;
	for (int i = 0; i < len - 1; ++i)
		for (int j = i + 1; j < len; ++j)
			if (opt[i] + 1 < opt[j])
			{
				valid = true;
				for (int k = i + 1; k < j; ++k)
					if (points[k]->dist_to_line(points[i], points[j]) > ERR)
					{
						valid = false;
						break;
					}
				if (valid)
				{
					opt[j] = opt[i] + 1;
					from[j] = i;
				}
			}

	for (int i = len - 1; i != 0; i = from[i])
		points[i]->reserve = true;
	points[0]->reserve = true;

	delete []opt;
	delete []from;
}

void recompress(std::vector<Point *> &points)
{
	// Window algorithms, recompress by Dynamic Programming
	std::vector<int> res_list;
	int st = 0, en, len = points.size();

	for (int i=0; i<len; ++i)
		if (i<len-1 && !points[i]->reserve && points[i+1]->reserve)
			res_list.push_back(i);
		else if (points[i]->reserve)
		{
			points[i]->reserve = false;
			res_list.push_back(i);
			if (i<len-1 && points[i+1]->reserve == false)
				res_list.push_back(i+1);
		}

	len = res_list.size();
	int *opt = new int[len], *from = new int[len];
	bool valid;
	memset(opt, 0x3f, sizeof(int)*len);

	opt[0] = 1;
	for (int i = 0; i < len - 1; ++i)
		for (int j = i + 1; j < len; ++j)
			if (opt[i] + 1 < opt[j])
			{
				valid = true;
				st = res_list[i];
				en = res_list[j];
				for (int k = st + 1; k < en; ++k)
					if (points[k]->dist_to_line(points[st], points[en]) > ERR)
					{
						valid = false;
						break;
					}
				if (valid)
				{
					opt[j] = opt[i] + 1;
					from[j] = i;
				}
			}

	for (int i = len - 1; i != 0; i = from[i])
		points[res_list[i]]->reserve = true;
	points[0]->reserve = true;

	delete []opt;
	delete []from;
}


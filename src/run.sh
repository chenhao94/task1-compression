echo 'Converting data'
ruby -w data_processing.rb
cp ../data/*.log ./

echo 'Compression begin...'
./main 2007-10-14-GPS_done.log compressed_t_dop.txt 0 0 &
PID0=$!
#./main 2007-10-14-GPS_done.log compressed_s_dop.txt 1 0
./main 2007-10-14-GPS_done.log recompressed_t_dop.txt 0 0 1 &
PID1=$!
#./main 2007-10-14-GPS_done.log recompressed_s_dop.txt 1 0 1
./main 2007-10-14-GPS_done.log compressed_t_sw.txt 0 1 &
PID2=$!
#./main 2007-10-14-GPS_done.log compressed_s_sw.txt 1 1
./main 2007-10-14-GPS_done.log recompressed_t_sw.txt 0 1 1 &
PID3=$!
#./main 2007-10-14-GPS_done.log recompressed_s_sw.txt 1 1 1
./main 2007-10-14-GPS_done.log compressed_t_ow.txt 0 2 &
PID4=$!
#./main 2007-10-14-GPS_done.log compressed_s_ow.txt 1 2
./main 2007-10-14-GPS_done.log recompressed_t_ow.txt 0 2 1 &
PID5=$!
#./main 2007-10-14-GPS_done.log recompressed_s_ow.txt 1 2 1
#echo 'Oracle best (temporal)...'
#./main 2007-10-14-GPS_done.log compressed_t_dyp.txt 0 3
#echo 'Oracle best (spatio)...'
#./main 2007-10-14-GPS_done.log compressed_s_dyp.txt 1 3
wait $PID0 $PID1 $PID2 $PID3 $PID4 $PID5

ruby -w output_convert_back.rb compressed_t_dop.txt
PID0=$!
ruby -w output_convert_back.rb recompressed_t_dop.txt
PID1=$!
ruby -w output_convert_back.rb compressed_t_sw.txt
PID2=$!
ruby -w output_convert_back.rb recompressed_t_sw.txt
PID3=$!
ruby -w output_convert_back.rb compressed_t_ow.txt
PID4=$!
ruby -w output_convert_back.rb recompressed_t_ow.txt
PID5=$!
wait $PID0 $PID1 $PID2 $PID3 $PID4 $PID5

echo 'compressed complete.'

#ifndef __POINT_H__
#define __POINT_H__
#include "Distance.h"
#include "Param.h"

#define m_sqr(x) ((x)*(x))

struct Point
{
	unsigned id;
	int t;
	double x,y;
	bool reserve;

	Point(unsigned _id, int _t=0, double _x=0, double _y=0): id(_id), t(_t), x(_x), y(_y), reserve(false) {}

	double magnitude(Point *p) const { return m_sqr(x-p->x) + m_sqr(y-p->y); }

	Point* s_project(Point *p1, Point *p2);
	Point* t_project(Point *p1, Point *p2);
	
	Point* project(Point *p1, Point *p2) 
	// Choosing different ways of projection
	{ 
		if (PROJ_TYPE == 0)
			return t_project(p1, p2);
		return s_project(p1, p2);
	}

	double dist_to_line(Point *p1, Point *p2);
};

#endif

require 'csv'
load 'data_converter.rb'
Dir.glob('../data/raw/*-GPS.log') do |file|
	puts file
	out_addr = "../data" + file[11 .. file.length-5] + "_done.log"
	out = File.open(out_addr, "w")
	last_timestamp = -1000000000
	last_lon = 0
	last_lat = 0
	raw = File.open(file)
	raw.each{ |line|
		row = line.split
		next if row.length < 11
		next if row[3].to_f.abs < 0.1
		next if row[5].to_f.abs < 0.1
		timestamp = get_timestamp("#{row[1]} #{row[2]}")
		lon = lon_to_f(row[3])
		lat = lat_to_f(row[5])
		if (timestamp > last_timestamp)
			velo = ((lon - last_lon).abs + (lat - last_lat).abs)/(timestamp - last_timestamp)
		else next end
		next if last_timestamp != 1000000000 &&	velo > 600 
		last_timestamp = timestamp
		last_lon = lon
		last_lat = lat
		out.printf("%d %f %f\n", timestamp.to_i, lon.to_f, lat.to_f)
	}
	raw.close
	out.close
end

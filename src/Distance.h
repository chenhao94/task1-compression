#ifndef ____DISTANCE_H___
#define ____DISTANCE_H___
#include <cmath>

static const double PI = 3.1415926535897932384626;

double geo_dist(double lon1, double lat1, double lon2, double lat2);

inline double rad(double d)
{
	return d*PI/180.;
}

inline double x_to_lon(double x)
{
	return 116+x/100000.;
}

inline double y_to_lat(double y)
{
	return 36+y/100000.;
}

inline double x_y_dist(double x1, double y1, double x2, double y2)
{
	return geo_dist(x_to_lon(x1), y_to_lat(y1), x_to_lon(x2), y_to_lat(y2));
}

#endif

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>

#include "Point.h"
#include "Distance.h"
#include "Compress.h"

using namespace std;

int ERR;
int PROJ_TYPE, COMP_ALOG;
bool RECOMPRESS;

int main(int argc, char *argv[])
{
	string filename = "data.log";
	string target = "data.compressed";
	ERR = 30;
	PROJ_TYPE = COMP_ALOG = 0;
	RECOMPRESS = false;
	if (argc > 1)
		filename = argv[1];
	if (argc > 2)
		target = argv[2];
	if (argc > 4)
	{
		PROJ_TYPE = argv[3][0] - '0';
		COMP_ALOG = argv[4][0] - '0';
	}
	if (argc > 5)
		RECOMPRESS = argv[5][0] - '0';
	if (argc > 6)
	{
		ERR = 0;
		for (int i = 0; argv[6][i] != '\0'; ++i)
			ERR = ERR * 10 + argv[6][i]-'0';
	}

	vector<Point *> points;
	unsigned id = 0;
	int t;
	double x, y;

	freopen(filename.c_str(), "r", stdin);
	while (1)
	{
		cin >> t >> x >> y;
		if (cin.fail())
			break;
		points.push_back(new Point(id++,t,x,y));
	}	
	fclose(stdin);

	compress(points);
	freopen(target.c_str(), "w", stdout);
	for (vector<Point *>::iterator itr = points.begin(); itr != points.end(); ++itr)
		if ((*itr)->reserve)
			cout << (*itr)->id << " " << (*itr)->t << " " << (*itr)->x << " " << (*itr)->y << endl;
	fclose(stdout);

	return 0;
}

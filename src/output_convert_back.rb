load 'data_converter.rb'
raw_addr = ARGV[0]
out_addr = raw_addr.chomp("txt") + "log"

raw = File.open(raw_addr)
out = File.open(out_addr, "w")

raw.each{ |line|
	row = line.split
	out.printf("%d %s %f E %f N\n", row[0].to_i, int_to_time(row[1].to_i), f_to_lon(row[2].to_f), f_to_lat(row[3].to_f))
}

raw.close
out.close

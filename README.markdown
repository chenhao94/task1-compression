#Task 1 - Compression

A course project of Spatio-Temperal Analytics

##Build and run

If you have converted data file, then

	make
	./main [SOURCE_PATH] [TARGET_PATH] [PROJECTION_TYPE] [ALGORITHM] [RECOMPRESS] [ERROR]

You will get a compressed file in special format. You can use `data_processing.rb` and `output_convert_back.rb` to generate the data file and convert the compressed data back to the readable format.

A easier way is putting the raw data into `data/raw` then

	make
	./run.sh

You will get several compressed readable file, they are compressed by different compressing algorithms.

##Dependency
* g++
* ruby

##Options

###Projection Type

There are two ways to retrieve the point: projecting it on the line directly or concerning with the ratio of time. Therefore the values of the option are the following,

* 0: temporal way [DEFAULT]
* 1: spatio way

###Algorithms

You can choose one of the following algorithms,

* 0: Douglas-Peucker [DEFAULT]
* 1: Sliding Window
* 2: Open Window
* 3: Dynamic Programming

###Recompress

When you using window algorithm (1 or 2), you can set this option to 0(disable, default) or 1(enable) to choose recompressing or not.

###Error

An integer which is the upper bound of error. Default value is 30.


